﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Helicotper : MonoBehaviour {

	public float upForce;
	public float angle = 0.1f;
	Rigidbody rb;

	// Use this for initialization
	void Start () {
		//angle = 0.1f;
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//rb.AddForce(Vector3.up * upForce);
		rb.AddForce(transform.up * upForce);
	}

	//Управление
	void Update () {
		if (Input.GetKey(KeyCode.W)){
			transform.Rotate (angle, 0, 0);
		}
		
		if (Input.GetKey(KeyCode.S)){
			transform.Rotate (-angle, 0, 0);
		}
				
		if (Input.GetKey(KeyCode.A)){
			transform.Rotate (0, 0, angle);
		}
		
		if (Input.GetKey(KeyCode.D)){
			transform.Rotate (0, 0, -angle);
		}

		if (Input.GetKey(KeyCode.Q)){
			transform.Rotate (0, -0.5f, 0);
		}

		if (Input.GetKey(KeyCode.E)){
			transform.Rotate (0, 0.5f, 0);
		}
			
		if (Input.GetKey(KeyCode.UpArrow)){
			upForce += 0.1f;
		}

		if (Input.GetKey(KeyCode.DownArrow)){
			upForce -= 0.1f;
			if (upForce < 0) {
				upForce = 0;
			}
		}
	}
}
