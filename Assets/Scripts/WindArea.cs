﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindArea : MonoBehaviour {

	public float windStrength;
	public Vector3 windDirection;

	private GameObject windZone;

	Rigidbody rb;
	WindZone wz;

	//ParticleSystem ps;

	//List<ParticleSystem.Particle> inside = new List<ParticleSystem.Particle>();

	// Use this for initialization
	void Start () {
		//ps = GetComponent<ParticleSystem>();
		//windZone = transform.Find ("WindZone").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter (Collider col) {
		if (col.gameObject.tag == "Helicopter") {
			//wz = this.gameObject.GetComponentInChildren<WindZone> ();
			//wz.enabled = true;
			//windZone.gameObject.SetActive (true);
			//windZone.gameObject.active = true;
		}
	}

	void OnTriggerStay (Collider col) {
		//if (col.gameObject.tag == "Helicopter") {
			rb = col.GetComponent<Rigidbody> ();
			rb.AddForce (windDirection * windStrength);
			//col.gameObject.transform.Translate(windDirection);

			//string name = col.gameObject.name;
			//Debug.Log (name);
		//}
	}

	void OnTriggerExit (Collider col) {
		if (col.gameObject.tag == "Helicopter") {
			//windZone.gameObject.SetActive (false);
		}
	}

	/*
	void OnParticleCollision(GameObject other) {
		string name = other.gameObject.name;
		Debug.Log (name);
	}
	*/
}
