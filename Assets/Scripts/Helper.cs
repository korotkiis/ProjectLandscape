﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;
using System.IO;
using System;

public class Helper : MonoBehaviour {

	private string path = "Data/Save/Save.xml";
	private string pathTime = "Data/Save/SaveTime.xml";

	public List<SaveableObj> objects = new List<SaveableObj>();


	XElement root1 = new XElement ("SaveF");

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.F1))
			Save ();
		if (Input.GetKeyDown(KeyCode.F2))
			Load ();
		//SaveF ();
		root1.Add(Time.frameCount);
		foreach (SaveableObj obj in objects) 
		{
			root1.Add(obj.GetElement());
		}
		Debug.Log (root1);
		XDocument saveDoc = new XDocument (root1);
		string appendText = saveDoc.ToString () + Environment.NewLine; //+ saveDoc1.ToString();
		File.WriteAllText (pathTime, appendText);
	}



	/*private void Start ()
	{
		Update ();
		XDocument saveDoc = new XDocument (root1);
		string appendText = saveDoc.ToString () + Environment.NewLine; //+ saveDoc1.ToString();
		File.AppendAllText (pathTime, appendText);
	}*/
	public void Save()
	{
		XElement root = new XElement ("Save");

		foreach (SaveableObj obj in objects) 
		{
			root.Add(obj.GetElement());
		}

		Debug.Log (root);

		XDocument saveDoc = new XDocument (root);

		File.WriteAllText (path, saveDoc.ToString());
		Debug.Log (path);
	}

	public void SaveF()
	{
		
			XElement root = new XElement ("SaveF");
			foreach (SaveableObj obj in objects) 
			{
				
				
			root.Add(obj.GetElement());

			}
			Debug.Log (root);
			
			XDocument saveDoc = new XDocument (root);
			string appendText = saveDoc.ToString () + Environment.NewLine; //+ saveDoc1.ToString();
			File.AppendAllText (pathTime, appendText);
			
	}



	public void Load()
	{
		XElement root = null;
		if (!File.Exists (path)) {
			Debug.Log ("Save not found!");
			if (File.Exists ("Data/Save/Save.xml"))
				root = XDocument.Parse (File.ReadAllText ("Data/Save/Save.xml")).Element ("Save");
		} 
		else 
		{
			root = XDocument.Parse (File.ReadAllText (path)).Element ("Save");
			Debug.Log ("Load fine!");
		}

		if (root == null) 
		{
			Debug.Log ("Level fail");
			return;
		}

		GenerateScene (root);
	}

	private void GenerateScene(XElement root)
	{
		foreach(SaveableObj obj in objects)
		{
			obj.DestroySelf();
		}

		foreach (XElement instance in root.Elements("instance")) {
			Vector3 position = Vector3.zero;

			position.x = float.Parse(instance.Attribute ("x").Value);
			position.y = float.Parse(instance.Attribute ("y").Value);
			position.z = float.Parse(instance.Attribute ("z").Value);

			Instantiate (Resources.Load<GameObject> (instance.Value), position, Quaternion.identity);

		}
	}
}
