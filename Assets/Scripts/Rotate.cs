﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

	public float rotateSpeed = 1;
	Helicotper hl;

	// Use this for initialization
	void Start () {
		hl = GetComponentInParent<Helicotper> ();
	}
	
	// Update is called once per frame
	void Update () {
		rotateSpeed = hl.upForce;
		transform.Rotate (0, rotateSpeed, 0);
	}
}
