﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowParticleTriggers : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	ParticleSystem ps;

	List<ParticleSystem.Particle> inside = new List<ParticleSystem.Particle>();

	void OnEnable()
	{
		ps = GetComponent<ParticleSystem>();
	}

	void OnParticleTrigger()
	{
		int numInside = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Inside, inside);

		for (int i = 0; i < numInside; i++)
		{
			ParticleSystem.Particle p = inside[i];
			p.startColor = new Color32(255, 0, 0, 255);
			//p.velocity = new Vector3 (1, 0, 1);
			inside[i] = p;
		}
			
		ps.SetTriggerParticles(ParticleSystemTriggerEventType.Inside, inside);
	}
}
