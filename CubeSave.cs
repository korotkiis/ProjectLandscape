﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;

public class CubeSave : MonoBehaviour
{
    [SerializeField]
    private string objectName;
    private float gb;

    private GameHelper helper;

    private void Awake()
    {
        helper = FindObjectOfType<GameHelper>();
        
    }

    private void Start()
    {
        Debug.Log("My Debug.Log is " + this);
        helper.cubs.Add(this);
        gb = GameObject.FindGameObjectWithTag("Cube").GetComponent<Rigidbody>().mass;
    }


    public XElement GetCubeElement()
    {
        XAttribute x = new XAttribute("XPos", transform.position.x);
        XAttribute y = new XAttribute("YPos", transform.position.y);
        XAttribute z = new XAttribute("ZPos", transform.position.z);
        XAttribute mass = new XAttribute("Mass", gb);

        XElement element = new XElement("instance", objectName, x, y, z, mass);

        return element;

    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        helper.cubs.Remove(this);
    }

}

