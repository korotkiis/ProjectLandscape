﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml.Linq;
using System.IO;

public class GameHelper : MonoBehaviour
{

    private string path;

    public List<SaveableObject> objects = new List<SaveableObject>();
    public List<CubeSave> cubs = new List<CubeSave>();

    private void Awake()
    {
        path = Application.persistentDataPath + "/testSave.xml";     //C:/Users/Public/Documents/Unity Projects/Standard Assets Example Project/Saves
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            Save();
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            Load();
        }
    }

    public void Save()
    {
        XElement root = new XElement("OBJ");
        XElement cube = new XElement("Cube");
        //Debug.Log("First Root is    " + root);
        foreach (SaveableObject obj in objects)
        {
            root.Add(obj.GetElement());
        }
        foreach(CubeSave obj in cubs)
        {
            cube.Add(obj.GetCubeElement());
        }
        //Debug.Log("2 Root is    " + root);
        Debug.Log(root);
        Debug.Log("Cube " + cube);
       // Debug.Log("3 Root is    " + root);

        XDocument SaveDocument = new XDocument(root);
        XDocument SaveCube = new XDocument(cubs);

        //Debug.Log("4 Root is    " + root);
	string appendText = SaveDocument.ToString() + Enviroment.NewLine + SaveCube.ToString();
        File.WriteAllText(path, appendText);
        //File.AppendAllText(path, SaveCube.ToString());

        Debug.Log(path);
        Debug.Log("5 Root is    " + root);

    }

    public void Load()
    {
        XElement root = null;

        if (!File.Exists(path))
        {
            Debug.Log("Save data not found...  ");

            if (File.Exists(Application.persistentDataPath + "/level.xml"))
            {
                root = XDocument.Parse(File.ReadAllText(Application.persistentDataPath + "/level.xml")).Element("root");
            }
            else
            {
                Debug.Log("Level not founded");
            }
        }
        else
        {
            root = XDocument.Parse(File.ReadAllText(path)).Element("root");
        }

        if (root == null)
        {
            Debug.Log("Level load failed...  ");
            return;
        }

        Debug.Log("Loaded   " + root);
        GenerateScene(root);



    }

    private void GenerateScene(XElement root)
    {

        /*foreach(SaveableObject obj in objects)
        {
            obj.DestroySelf();
        }*/

        foreach (XElement instance in root.Elements("Instance"))
        {
            Vector3 position = Vector3.zero;

            position.x = float.Parse(instance.Attribute("x").Value);
            position.x = float.Parse(instance.Attribute("y").Value);
            position.y = float.Parse(instance.Attribute("z").Value);

            Instantiate(Resources.Load<GameObject>(instance.Value), position, Quaternion.identity);
        }
    }

}
